LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;

ENTITY SRAM_TB	IS
	PORT(
		CLOCK_IN	: IN	STD_LOGIC;
		RX			: IN	STD_LOGIC;
		TX			: OUT	STD_LOGIC;
		WE, OE, CE	: OUT	STD_LOGIC;
		SRM_ADD		: OUT	UNSIGNED(18 DOWNTO 0);
		DIO			: INOUT	UNSIGNED(07 DOWNTO 0)
	);
END SRAM_TB;

-- CLOCK RATE = 50MHz, EQUAL CLOCK PERIOD = 20 NS
-- THIS MODULE TEST SRAM_CNTR MODULE

ARCHITECTURE BEHAVIORAL	OF SRAM_TB	IS

	component ICON
	  PORT (
		CONTROL0 : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0));

	end component;
	
	component ILA
	  PORT (
		CONTROL : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0);
		CLK : IN STD_LOGIC;
		DATA : IN STD_LOGIC_VECTOR(35 DOWNTO 0);
		TRIG0 : IN STD_LOGIC_VECTOR(0 TO 0));

	end component;

	component CLOCK_50_200MHz
	port
	 (-- Clock in ports
	  CLK_IN           : in     std_logic;
	  -- Clock out ports
	  CLK_OUT_50          : out    std_logic;
	  CLK_OUT_200          : out    std_logic
	 );
	end component;

	SIGNAL CLOCK		: STD_LOGIC	:= '0';
	SIGNAL CLOCK_200	: STD_LOGIC	:= '0';
	
	SIGNAL CONTROL0		: STD_LOGIC_VECTOR(35 DOWNTO 0)	:= (OTHERS=>'0');
	SIGNAL ILA_DATA		: STD_LOGIC_VECTOR(35 DOWNTO 0)	:= (OTHERS=>'0');
	
	-- SRAM CONTROLLER
	SIGNAL MEM			: STD_LOGIC	:= '0';
	SIGNAL RW			: STD_LOGIC	:= '0';
	SIGNAL MEM_ADD		: UNSIGNED(18 DOWNTO 0)	:= (OTHERS=>'0');
	SIGNAL DATA_F2S		: UNSIGNED(07 DOWNTO 0)	:= (OTHERS=>'0');
	SIGNAL SRM_RDY		: STD_LOGIC	:= '0';
	SIGNAL WE_INT		: STD_LOGIC	:= '0';
	SIGNAL OE_INT		: STD_LOGIC	:= '0';
	SIGNAL CE_INT		: STD_LOGIC	:= '0';
	SIGNAL DATA_S2F		: UNSIGNED(07 DOWNTO 0)	:= (OTHERS=>'0');
	SIGNAL S2F_INT		: UNSIGNED(07 DOWNTO 0)	:= (OTHERS=>'0');
	SIGNAL SRM_ADD_INT	: UNSIGNED(18 DOWNTO 0)	:= (OTHERS=>'0');
	
	-- RS232 RX
	SIGNAL DATA_RX		: UNSIGNED(07 DOWNTO 0)	:= (OTHERS=>'0');
	SIGNAL VALID_RX		: STD_LOGIC	:= '0';
	SIGNAL RX_INT		: STD_LOGIC	:= '0';
	
	-- RS232 TX
	SIGNAL DATA_TX		: UNSIGNED(07 DOWNTO 0)	:= (OTHERS=>'0');
	SIGNAL SEND_TX		: STD_LOGIC	:= '0';
	SIGNAL BUSY_TX		: STD_LOGIC	:= '0';
	SIGNAL TX_INT		: STD_LOGIC	:= '0';
	
	-- INTERNAL PRACTICAL REGISTERS
	SIGNAL COUNTER		: UNSIGNED(18 DOWNTO 0)	:= (OTHERS=>'0');
	SIGNAL ERR_CNT		: UNSIGNED(07 DOWNTO 0)	:= (OTHERS=>'0');
	SIGNAL ERRORS		: UNSIGNED(18 DOWNTO 0)	:= (OTHERS=>'0');
	SIGNAL TMP_DATA		: UNSIGNED(03 DOWNTO 0)	:= (OTHERS=>'0');
	SIGNAL TMP_ADD1		: UNSIGNED(02 DOWNTO 0)	:= (OTHERS=>'0');
	SIGNAL TMP_ADD2		: UNSIGNED(07 DOWNTO 0)	:= (OTHERS=>'0');
	SIGNAL TRIG			: STD_LOGIC_VECTOR(0 DOWNTO 0)	:= (OTHERS=>'0');
	SIGNAL ADD_TRG		: UNSIGNED(18 DOWNTO 0)	:= (OTHERS=>'0');
	SIGNAL S2F_TRG		: UNSIGNED(07 DOWNTO 0)	:= (OTHERS=>'0');
	SIGNAL S2F_CHECK		: UNSIGNED(11 DOWNTO 0)	:= (OTHERS=>'0');
	SIGNAL FULL_WR		: STD_LOGIC	:= '0';
	SIGNAL FULL_RD		: STD_LOGIC	:= '0';
	SIGNAL SND_ERR		: STD_LOGIC	:= '0';
	SIGNAL SND_ERR_Z	: STD_LOGIC	:= '0';
	SIGNAL SND_ERR_Y	: STD_LOGIC	:= '0';
	SIGNAL SND_ERR_W	: STD_LOGIC	:= '0';
	SIGNAL SND_ERR2		: STD_LOGIC	:= '0';
	SIGNAL SND_ERR0		: STD_LOGIC	:= '0';
	SIGNAL COMP_ADD		: STD_LOGIC	:= '0';
	SIGNAL READING		: STD_LOGIC	:= '0';
	SIGNAL READ_CELL	: STD_LOGIC	:= '0';
	SIGNAL READ_CELL0	: STD_LOGIC	:= '0';
	SIGNAL READ_CELL_X	: STD_LOGIC	:= '0';

BEGIN

	WE		<= WE_INT;
	OE		<= OE_INT;
	CE		<= CE_INT;
	SRM_ADD	<= SRM_ADD_INT;
	
	S2F_CHECK	<= DATA_TX & READ_CELL & READ_CELL_X & BUSY_TX & SEND_TX;

	PROCESS(CLOCK)
	BEGIN
	
		IF RISING_EDGE(CLOCK)	THEN
		
			RX_INT	<= RX;
			TX		<= TX_INT;
			
			SEND_TX	<= '0';
			MEM		<= '0';
			S2F_INT	<= DATA_S2F;
			
			ADD_TRG	<= MEM_ADD;
			S2F_TRG	<= DATA_S2F;
			
			
			IF VALID_RX='1'	THEN
				
				COUNTER		<= (OTHERS=>'0');
				-- WRITE ENTIRE SRAM
				IF DATA_RX=X"77"	AND COMP_ADD='0'	THEN
					MEM_ADD		<= (OTHERS=>'0');
					DATA_F2S	<= (OTHERS=>'1');
					COUNTER		<= (0=>'1', OTHERS=>'0');
					MEM			<= '1';
					RW			<= '0';
					FULL_WR		<= '1';
				END IF;
				
				-- READ ENTIRE SRAM
				IF DATA_RX=X"55"	AND COMP_ADD='0'	THEN
					MEM_ADD		<= (OTHERS=>'0');
					ERRORS		<= (OTHERS=>'0');
					MEM			<= '1';
					RW			<= '1';
					FULL_RD		<= '1';
				END IF;
				
				-- READ SPECIFIC CELL
				IF DATA_RX(7 DOWNTO 4)=X"3"	AND COMP_ADD='0'	THEN
					COUNTER		<= COUNTER + 1;
					IF COUNTER=TO_UNSIGNED(0, 19)	THEN
						MEM_ADD(18 DOWNTO 16)		<= DATA_RX(02 DOWNTO 0);
					END IF;
					IF COUNTER=TO_UNSIGNED(1, 19)	THEN
						MEM_ADD(15 DOWNTO 12)		<= DATA_RX(03 DOWNTO 0);
					END IF;
					IF COUNTER=TO_UNSIGNED(2, 19)	THEN
						MEM_ADD(11 DOWNTO 8)		<= DATA_RX(03 DOWNTO 0);
					END IF;
					IF COUNTER=TO_UNSIGNED(3, 19)	THEN
						MEM_ADD(07 DOWNTO 4)		<= DATA_RX(03 DOWNTO 0);
					END IF;
					IF COUNTER=TO_UNSIGNED(4, 19)	THEN
						MEM_ADD(03 DOWNTO 0)		<= DATA_RX(03 DOWNTO 0);
						COUNTER						<= (OTHERS=>'0');
						MEM							<= '1';
						RW							<= '1';
						READ_CELL0					<= '1';
					END IF;
				END IF;
				
				-- INSERT VALUE ON SPECIFIED ADDRESS
				IF DATA_RX(7)='1'	AND COMP_ADD='0'	THEN
					TMP_DATA	<= DATA_RX(6 DOWNTO 3);
					TMP_ADD1	<= DATA_RX(2 DOWNTO 0);
					COMP_ADD	<= '1';
				END IF;
				
				-- COMPLETE ADDRESS TO INSERT VALUE
				IF COMP_ADD='1'	THEN
					COUNTER		<= COUNTER + 1;
					TMP_ADD2	<= DATA_RX;
					IF COUNTER(0)='1'	THEN
						MEM_ADD		<= TMP_ADD1 & TMP_ADD2 & DATA_RX;
						DATA_F2S	<= (3=>TMP_DATA(3), 2=>TMP_DATA(2), 1=>TMP_DATA(1), 0=>TMP_DATA(0), OTHERS=>'0');
						MEM			<= '1';
						RW			<= '0';
						COMP_ADD	<= '0';
						COUNTER		<= (OTHERS=>'0');
					END IF;
				END IF;
				
			END IF;
			
			IF READ_CELL0='1'	THEN
				READ_CELL0		<= '0';
				READ_CELL		<= '1';
			END IF;
			IF READ_CELL='1'	THEN
				DATA_TX		<= DATA_S2F;
				SEND_TX		<= '1';
				READ_CELL	<= '0';
				READ_CELL_X	<= '1';
			END IF;
			IF READ_CELL_X='1'	THEN
				COUNTER		<= COUNTER + 1;
				IF COUNTER(1)='1'	THEN
					READ_CELL_X		<= '0';
					COUNTER			<= (OTHERS=>'0');
					IF BUSY_TX='0'	THEN
						DATA_TX		<= S2F_INT;
						SEND_TX		<= '1';
					END IF;
				END IF;
			END IF;
			
			IF FULL_WR='1'	THEN
				MEM			<= '1';
				MEM_ADD		<= COUNTER;
				COUNTER		<= COUNTER + 1;
				DATA_F2S	<= NOT COUNTER(7 DOWNTO 0);
				IF COUNTER=TO_UNSIGNED(2**19-1, 19)	THEN
					FULL_WR		<= '0';
					DATA_TX		<= X"EE";
					SEND_TX		<= '1';
				END IF;
			END IF;
			
			IF FULL_RD='1'	THEN
				MEM			<= '1';
				ERR_CNT		<= COUNTER(7 DOWNTO 0);	-- ERR_CNT = COUNTER_PREV
				COUNTER		<= MEM_ADD;	-- COUNTER = MEM_ADD_PREV
				MEM_ADD		<= MEM_ADD + 1;
				IF COUNTER(0)='1'	THEN
					READING		<= '1';
				END IF;
				IF COUNTER=TO_UNSIGNED(0, 19)	AND MEM_ADD=TO_UNSIGNED(1, 19)	AND 
						ERR_CNT=X"FF"	THEN
					FULL_RD		<= '0';
					READING		<= '0';
					SND_ERR0	<= '1';
					COUNTER		<= (OTHERS=>'0');
				END IF;
			END IF;
			
			IF READING='1'	THEN
				IF S2F_INT/=(NOT ERR_CNT(7 DOWNTO 0))	THEN
					ERRORS		<= ERRORS + 1;
				END IF;
			END IF;
			
			IF SND_ERR0='1'	THEN
				IF BUSY_TX='0'	THEN
					COUNTER		<= COUNTER + 1;
					IF COUNTER(3)='1'	THEN
						DATA_TX		<= X"A" & "0" & ERRORS(18 DOWNTO 16);
						SEND_TX		<= '1';
						SND_ERR0	<= '0';
						SND_ERR_W	<= '1';
						COUNTER		<= (OTHERS=>'0');
					END IF;
				END IF;
			END IF;
			IF SND_ERR_W='1'	THEN
				COUNTER		<= COUNTER + 1;
				IF COUNTER(1)='1'	THEN
					SND_ERR_W		<= '0';
					COUNTER			<= (OTHERS=>'0');
					SND_ERR			<= '1';
					IF BUSY_TX='0'	THEN
						DATA_TX		<= X"A" & "0" & ERRORS(18 DOWNTO 16);
						SEND_TX		<= '1';
					END IF;
				END IF;
			END IF;
			
			IF SND_ERR='1'	THEN
				IF BUSY_TX='0'	THEN
					COUNTER		<= COUNTER + 1;
					IF COUNTER(3)='1'	THEN
						DATA_TX		<= ERRORS(15 DOWNTO 8);
						SEND_TX		<= '1';
						SND_ERR		<= '0';
						SND_ERR_Z	<= '1';
						--SND_ERR2	<= '1';
						COUNTER		<= (OTHERS=>'0');
					END IF;
				END IF;
			END IF;
			IF SND_ERR_Z='1'	THEN
				COUNTER		<= COUNTER + 1;
				IF COUNTER(1)='1'	THEN
					SND_ERR_Z		<= '0';
					COUNTER			<= (OTHERS=>'0');
					SND_ERR2		<= '1';
					IF BUSY_TX='0'	THEN
						DATA_TX		<= ERRORS(15 DOWNTO 8);
						SEND_TX		<= '1';
					END IF;
				END IF;
			END IF;
			
			IF SND_ERR2='1'	THEN
				IF BUSY_TX='0'	THEN
					COUNTER		<= COUNTER + 1;
					IF COUNTER(3)='1'	THEN
						DATA_TX		<= ERRORS(7 DOWNTO 0);
						SEND_TX		<= '1';
						SND_ERR2	<= '0';
						SND_ERR_Y	<= '1';
						COUNTER		<= (OTHERS=>'0');
					END IF;
				END IF;
			END IF;
			IF SND_ERR_Y='1'	THEN
				COUNTER		<= COUNTER + 1;
				IF COUNTER(1)='1'	THEN
					SND_ERR_Y		<= '0';
					COUNTER			<= (OTHERS=>'0');
					IF BUSY_TX='0'	THEN
						DATA_TX		<= ERRORS(07 DOWNTO 0);
						SEND_TX		<= '1';
					END IF;
				END IF;
			END IF;
		
		END IF;	-- RISING EDGE
	
	END PROCESS;

	SRAM_CNTR_INS:	ENTITY WORK.SRAM_CNTR(BEHAVIORAL)
	PORT MAP(
		CLOCK_200, MEM, RW, MEM_ADD, DATA_F2S, SRM_RDY, WE_INT,
		OE_INT, CE_INT, DATA_S2F, SRM_ADD_INT, DIO
	);

	RS232_RX_INS:	ENTITY WORK.RS_232_RX(BEHAVIORAL)
	PORT MAP(
		CLOCK, DATA_RX, VALID_RX, RX_INT
	);

	RS232_TX_INS:	ENTITY WORK.RS_232_TX(BEHAVIORAL)
	PORT MAP(
		CLOCK, DATA_TX, SEND_TX, BUSY_TX, TX_INT
	);
	
	REQUIRED_CLOCKS : CLOCK_50_200MHz
	  port map
	   (-- Clock in ports
		CLK_IN => CLOCK_IN,
		-- Clock out ports
		CLK_OUT_50 => CLOCK,
		CLK_OUT_200 => CLOCK_200);
		
	ICON_INS : ICON
	  port map (
		CONTROL0 => CONTROL0);

	TRIG(0)		<= MEM;
	ILA_DATA	<= STD_LOGIC_VECTOR(S2F_CHECK) & 
					'0' & STD_LOGIC_VECTOR(ADD_TRG(6 DOWNTO 0)) & 
					STD_LOGIC_VECTOR(DATA_S2F) & STD_LOGIC_VECTOR(DIO);
		
	ILA_INS : ILA
	  port map (
		CONTROL => CONTROL0,
		CLK => CLOCK_200,
		DATA => ILA_DATA,
		TRIG0 => TRIG);
	
END BEHAVIORAL;